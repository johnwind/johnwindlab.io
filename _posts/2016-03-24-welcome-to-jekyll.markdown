---
layout: post
title:  "they did what ?"
date:   2016-03-24 15:32:14 -0300
categories: jekyll update
---

![an image alt text]({{ site.baseurl }}/images/dd_fish.png "an image title")

[ksh](https://man.openbsd.org/ksh.1) doesn't provide the 'out of the box' approach, but it does
have the smallest code base among the rest of the shell. 

i'm going to try and make it my default goto.



[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
